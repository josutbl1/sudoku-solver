from copyreg import constructor


class Cell:
    def __init__(self, value, index):
        self.value = value
        self.entropy = []
        self.entropy_level = -1
        self.index = index
        self.col = index%9
        self.row = int(index/9)
        self.square = int(self.col/3) + int(self.row/3)*3

    def getValue(self):
        return self.value

    def getCol(self):
        return self.col
    
    def getRow(self):
        return self.row

    def getSquare(self):
        return self.square

    def isCollapsed(self):
        return self.value is not 0

    def getLevel(self):
        return self.entropy_level

    def setEntropy(self, entropies):
        self.entropy = entropies
        self.entropy_level = len(entropies)

    def collapse(self):
        self.value = self.entropy[0]
        self.entropy_level = -1