from cell import Cell

def getLevel(cell):
    return cell.getLevel()

class Sudoku:
    def __init__(self, input):
        self.cells = []
        for i in range(81):
            self.cells.append(Cell(input[i], i))
        self.not_collapsed_count = 0
        self.calcEntropy()


    def calcEntropy(self):
        self.not_collapsed_count = 0
        for cell in self.cells:
            if not cell.isCollapsed():
                possible = [x for x in range(1, 10)]
                in_square = []
                in_row = []
                in_col = []
                for other in self.cells:
                    if other.isCollapsed():
                        ov = other.getValue()
                        if cell.getSquare() == other.getSquare():
                            in_square.append(ov)
                        if cell.getRow() == other.getRow():
                            in_row.append(ov)
                        if cell.getCol() == other.getCol():
                            in_col.append(ov)
                for x in in_square:
                    if x in possible:
                        possible.remove(x)
                for x in in_row:
                    if x in possible:
                        possible.remove(x)
                for x in in_col:
                    if x in possible:
                        possible.remove(x)
                cell.setEntropy(possible)
                self.not_collapsed_count += 1

    def solve(self):
        while self.not_collapsed_count > 0:
            was_there_a_change = False
            for cell in self.cells:
                if cell.getLevel() is 1:
                    cell.collapse()
                    self.not_collapsed_count -= 1
                    was_there_a_change = True
            if not was_there_a_change:
                print("Needs logic")
                break
            else:
                self.calcEntropy()
        if self.not_collapsed_count is 0:
            print("Solved!\n")

    def __str__(self):
        s = ""
        for i in range(81):
            s += str(self.cells[i].getValue()) + " "
            if (i+1)%9 == 0 and i != 0:
                s += "\n"

        return s
            