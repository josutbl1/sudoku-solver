import cv2 as cv
from sudoku import Sudoku

input =  [6, 0, 0, 0, 9, 3, 2, 0, 7,
          0, 4, 8, 6, 1, 7, 0, 0, 3,
          3, 7, 0, 0, 0, 0, 8, 6, 0,
          0, 0, 1, 2, 0, 6, 0, 3, 0,
          9, 0, 0, 1, 0, 8, 0, 7, 6,
          7, 0, 0, 3, 4, 9, 0, 0, 5,
          8, 6, 0, 0, 0, 0, 7, 0, 0,
          0, 9, 0, 0, 6, 5, 0, 8, 2,
          0, 0, 0, 4, 8, 2, 6, 0, 9]

sudoku = Sudoku(input)
sudoku.solve()
print(sudoku)
